SRC_F = $(wildcard *.f90)


all: $(SRC_F) 
	 gfortran -g -O3 -o main Tparam.f90 func.f90 cube_integral.f90 norms.f90 main.f90 -L ./library/ -lskit -llapack -lblas
clean: 
	rm -f *.o main	
