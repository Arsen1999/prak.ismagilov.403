real*8 function cube_integral(A, B, C, func)
    use Tparameters
    implicit none
    !  D.___.C    !
    !   |  /|     !
    !   | / |     !
    ! A.|/__|.B   !
    integer :: i
    real*8 :: mes, A(2), B(2), C(2), x, y, func_res, res
    external func
    
    func_res = 0d0
    res = 0d0
    ! A - x1,y3 B - x2,y3 C - x3,y3
    mes = 0.5 * dabs( (B(1) - A(1)) * (C(2) - A(2)) - (C(1) - A(1)) * (B(2) - A(2)) )
    !mes = 1.0 / (2 * n**2)
    do i = 1, m
        x = A(1) * n_i(1, i) + B(1) * n_i(2, i) + C(1) * n_i(3, i)
        y = A(2) * n_i(1, i) + B(2) * n_i(2, i) + C(2) * n_i(3, i)
        if (i .lt. 4) then 
            call func(x, y, func_res)
            res = res + w_i(1) * func_res
            !res = res + w_i(1) * func(x, y)
        else 
            call func(x, y, func_res)
            res = res + w_i(2) * func_res
            !res = res + w_i(2) * func(x, y)
        endif
    end do
    res = mes * res
    cube_integral = res
    
end function cube_integral

subroutine cube_integral_coeff()
    use TParameters
    implicit none
    ! m always equal 9 !
    
    ! w_i !
    w_i(1) = 0.205950504760887
    w_i(2) = 0.063691414286223

    ! n_i !
    n_i(:, 1) = (/0.124949503233232, 0.437525248383384, 0.437525248383384/)
    n_i(:, 2) = (/0.437525248383384, 0.124949503233232, 0.437525248383384/)
    n_i(:, 3) = (/0.437525248383384, 0.437525248383384, 0.124949503233232/)
    
    n_i(:, 4) = (/0.797112651860071, 0.165409927389841, 0.037477420750088/)
    n_i(:, 5) = (/0.165409927389841, 0.797112651860071, 0.037477420750088/)
    n_i(:, 6) = (/0.165409927389841, 0.037477420750088, 0.797112651860071/)
    n_i(:, 7) = (/0.797112651860071, 0.037477420750088, 0.165409927389841/)
    n_i(:, 8) = (/0.037477420750088, 0.797112651860071, 0.165409927389841/)
    n_i(:, 9) = (/0.037477420750088, 0.165409927389841, 0.797112651860071/)
    
end subroutine cube_integral_coeff

!subroutine del_cube_coeff(n_i, w_i)
!    implicit none
!    real*8 :: n_i(3, 9), w_i(2)
!    
!    deallocate(n_i, w_i)
!
!end subroutine del_cube_coeff

subroutine dots(x, y, A, B, C, D)
    use TParameters
    implicit none
    !  D.___.C    !
    !   |  /|     !
    !   | / |     !
    ! A.|/__|.B   !
    real*8 :: x, y, A(2), B(2), C(2), D(2)
    
    if (num == 1) then 
        A = (/x - h, y - h /)
        B = (/x, y - h /)
        C = (/x, y /)
        D = (/x - h, y /)
    else if (num == 2) then
        A = (/x, y - h /)
        B = (/x + h, y - h /)
        C = (/x + h, y /)
        D = (/x, y /)
    else if (num == 3) then
        A = (/x, y /)
        B = (/x + h, y /)
        C = (/x + h, y + h /)
        D = (/x, y + h /)
    else if (num == 4) then
        A = (/x - h, y /)
        B = (/x, y /)
        C = (/x, y + h /)
        D = (/x - h, y + h /)
    end if

end subroutine dots

real*8 function scalar_f_phi(x, y)
    use TParameters
    implicit none
    
    real*8 :: x, y, res, A(2), B(2), C(2), D(2), cube_integral
    integer :: i, j, num_temp
    external f_phi
    
    i = i_1
    j = j_1
    num_temp = num
    res = 0d0
    
    	
    if ((i > 2) .AND. (i < n + 1) .AND. (j > 2) .AND. (j < n + 1)) then ! inside
        do num = 1, 4
            call dots(x, y, A, B, C, D)
            res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
            !print *, A, B, C, D
        end do
    else if ((i == 2) .AND. (j == 2)) then ! corner down left
        do num = 1, 4
            call dots(x, y, A, B, C, D)
            res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
        end do
    else if ((i == 2) .AND. (j > 2) .AND. (j < n + 1)) then ! left
        do num = 1, 4
            call dots(x, y, A, B, C, D)
            res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
            !print *, 'res', res
        end do
    else if ((j == 2) .AND. (i > 2) .AND. (i < n + 1)) then ! down
        do num = 1, 4
            call dots(x, y, A, B, C, D)
            res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
        end do
    else if ((i == 2) .AND. (j == n + 1)) then ! corner up left
        do num = 1, 2
            call dots(x, y, A, B, C, D)
            res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
        end do
    else if ((i > 2) .AND. (j == n + 1)) then ! up
        do num = 1, 2
            call dots(x, y, A, B, C, D)
            res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
        end do
    else if ((j == 2) .AND. (i == n + 1)) then ! corner down right
        num = 4
        call dots(x, y, A, B, C, D)
        res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
        num = 1 
        call dots(x, y, A, B, C, D)
        res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
    else if ((j > 2) .AND. (i == n + 1)) then ! right
        num = 1
        call dots(x, y, A, B, C, D)
        res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
        num = 4
        call dots(x, y, A, B, C, D)
        res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
    else if ((j == n + 1) .AND. (i == n + 1)) then ! corner up right
        num = 1
        call dots(x, y, A, B, C, D)
        res = res + cube_integral(A, B, C, f_phi) + cube_integral(A, D, C, f_phi)
    end if
    num = num_temp
    scalar_f_phi = res
end function scalar_f_phi

real*8 function scalar_d_grad_grad(x, y)
    use TParameters
    implicit none
    ! mode = 1 if matX  !
    ! mode = 0 if g_d   !
    real*8 :: x, y, res, A(2), B(2), C(2), D(2), res_temp, res_square, cube_integral
    integer :: i, j, num_temp
    external d_grad_grad
    
    i = i_1
    j = j_1
    num_temp = num
    res = 0d0
    res_square = 0d0
    res_temp = 0d0

    if (mode == 1) then
        if ((i == 1) .AND. (j == 1)) then ! corner
            num = 3
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if 
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1  
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i == 1) .AND. (j > 1) .AND. (j < n + 1)) then ! left
            num = 3
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 2 
            call dots(x, y, A, B, C, D)
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i == 1) .AND. (j == n + 1)) then ! corner up left
            num = 2
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then    
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i > 1) .AND. (i < n + 1) .AND. (j == n + 1)) then ! up
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 2
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i == n + 1) .AND. (j == n + 1)) then ! corner up right
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i == n + 1) .AND. (j > 1) .AND. (j < n + 1)) then ! right
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 4
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i == n + 1) .AND. (j == 1)) then ! corner down right
            num = 4
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((j == 1) .AND. (i > 1) .AND. (i < n + 1)) then ! down
            num = 4
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 3
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        else if ((i > 1) .AND. (i < n + 1) .AND. (j > 1) .AND. (j < n + 1)) then ! inside
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 2
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 3
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i + 1) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if 
            if ((k == i + 1) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            
            num = 4
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j)) then
                num_k_l = 4
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i - 1) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
            if ((k == i) .AND. (l == j + 1)) then
                num_k_l = 1
                suitable_point = 1
                res = res + cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
            end if
        end if
    !!!!!!!!! G_D !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    else if (mode == 0) then
        if ((i == 2) .AND. (j == 2)) then ! corner
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            
	        num = 4
	        call dots(x, y, A, B, C, D)
	        if ((k == i - 1) .AND. (l == j)) then
	            num_k_l = 3
	            suitable_point = 1
	            res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
	            res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            
            num = 2
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
	            res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i + 1) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
	            res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            !print *, 'zna4', res, i, j, k, l
        else if ((i > 2) .AND. (i < n + 1) .AND. (j == 2)) then !down
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            num = 2
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i + 1) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
        else if ((i == n + 1) .AND. (j == 2)) then ! corner
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i) .AND. (l == j - 1)) then
                num_k_l = 4
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
        else if ((i == 2) .AND. (j > 2) .AND. (j < n + 1)) then ! left
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            num = 4
            call dots(x, y, A, B, C, D)
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j + 1)) then
                num_k_l = 2
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
        else if ((i == 2) .AND. (j == n + 1)) then ! corner
            num = 1
            call dots(x, y, A, B, C, D)
            if ((k == i - 1) .AND. (l == j)) then
                num_k_l = 2
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
            if ((k == i - 1) .AND. (l == j - 1)) then
                num_k_l = 3
                suitable_point = 1
                res_square = cube_integral(A, B, C, d_grad_grad) + cube_integral(A, D, C, d_grad_grad)
                call g_d((k - 1) * h, (l - 1) * h, res_temp)
                res = res + res_square * res_temp 
            end if
        end if
    end if
    
    num = num_temp
    scalar_d_grad_grad = res
end function scalar_d_grad_grad

real*8 function int_g_n_phi(x, y)
    use TParameters
    implicit none
    
    real*8 :: x, y, res, A(2), B(2), C(2), D(2), res_temp_1, res_temp_2
    integer :: i, j, num_temp
    
    i = i_1
    j = j_1
    res = 0d0
    res_temp_1 = 0d0
    res_temp_2 = 0d0
    num_temp = num
    
    if ((i == 2) .AND. (j == n + 1)) then ! corner up left
        num = 2
            call g_n_phi_ny(x + h, y, res_temp_1)
            call g_n_phi_ny(x, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
        num = 1
            call g_n_phi_ny(x, y, res_temp_1)
            call g_n_phi_ny(x - h, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
    else if ((i > 2) .AND. (i < n + 1) .AND. (j == n + 1)) then ! up
        num = 1
            call g_n_phi_ny(x, y, res_temp_1)
            call g_n_phi_ny(x - h, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
        num = 2
            call g_n_phi_ny(x + h, y, res_temp_1)
            call g_n_phi_ny(x, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
    else if ((i == n + 1) .AND. (j == n + 1)) then ! corner up right
        num = 1
            call g_n_phi_ny(x, y, res_temp_1)
            call g_n_phi_ny(x - h, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
            call g_n_phi_nx(x, y, res_temp_1)
            call g_n_phi_nx(x, y - h, res_temp_2)
            res = res + res_temp_1 - res_temp_2
    else if ((j > 2) .AND. (j < n + 1) .AND. (i == n + 1)) then ! right
        num = 1
            call g_n_phi_nx(x, y, res_temp_1)
            call g_n_phi_nx(x, y - h, res_temp_2)
            res = res + res_temp_1 - res_temp_2
        num = 4
            call g_n_phi_nx(x, y + h, res_temp_1)
            call g_n_phi_nx(x, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
            !print *, i, j, res
    else if ((j == 2) .AND. (i == n + 1)) then ! corner down right
        num = 4
            call g_n_phi_nx(x, y + h, res_temp_1)
            call g_n_phi_nx(x, y, res_temp_2)
            res = res + res_temp_1 - res_temp_2
        num = 1
            call g_n_phi_nx(x, y, res_temp_1)
            call g_n_phi_nx(x, y - h, res_temp_2)
            res = res + res_temp_1 - res_temp_2
    end if
    num = num_temp
    int_g_n_phi = res

end function int_g_n_phi

