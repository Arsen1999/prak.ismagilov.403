real*8 function c_h_norm(C_i_j, C_size)
    use TParameters
    implicit none
    
    integer :: C_size, i, j, n_loc
    real*8 :: C_i_j(C_size * C_size), err_c, res, err_c_temp
    
    open(1, file = 'c_h.txt', status = 'old')
    
    err_c = 0d0
    n_loc = C_size - 1
    do j = 1, n_loc + 1
        do i = 1, n_loc + 1 
            res = 0d0
            call g_d((i - 1) * h, (j - 1) * h, res)
		    !print *, 'g_d', res, 'C_i_j', C_i_j((j-2)*n + i - 1)
            err_c_temp = dabs(C_i_j((j - 1) * (n_loc + 1) + i) - res)
            write(1,*) (i-1)*h, (j-1)*h, err_c_temp
            if (err_c_temp > err_c) then
                err_c = err_c_temp
            end if 
        end do
    end do
    close(1)
    c_h_norm = err_c
end function c_h_norm


real*8 function l_h_norm(C_i_j, C_size)
    use TParameters
    implicit none
    external func_l_h
    
    integer :: C_size, n_loc
    real*8 :: C_i_j(C_size * C_size), res, res_temp, A(2), B(2), C(2), D(2), cube_integral, x, y

    n_loc = C_size  
    res = 0d0
    A = 0d0
    B = 0d0
    C = 0d0
    D = 0d0
    do j_1 = 1, n_loc - 1
        y = (j_1 - 1) * h 
        do i_1 = 1, n_loc - 1
            x = (i_1 - 1) * h
            num = 3 
            call dots(x, y, A, B, C, D)
            !print *, A, B, C, D
            res = res + cube_integral(A, B, C, func_l_h) + cube_integral(A, D, C, func_l_h)
        end do
    end do
    res_temp = dsqrt(res)
    l_h_norm = res_temp
end function l_h_norm
