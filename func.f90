subroutine f(x, y, res)
    use TParameters
    implicit none
    
    real*8 :: x, y, res

    res = 1.0 * pi * pi * sin(x * pi) * cos(y * pi) * (1.0 + eps) 
end subroutine f

subroutine f_phi(x, y, res)
    use TParameters
    implicit none
    
    real*8 :: x, y, res, res_1, res_2, x_i, y_j
    
    res_1 = 0d0
    res_2 = 0d0
    x_i = (i_1 - 1) * h
    y_j = (j_1 - 1) * h
    call f(x_i, y_j, res_1)
    call phi(x, y, res_2)    
    res = res_1 * res_2
    
end subroutine f_phi

subroutine g_n_phi_nx(x, y, res) ! right
    use TParameters
    implicit none
    
    real*8 :: x, y, res
    integer :: i, j
    
    i = i_1
    j = j_1
    res = 0d0
    
    if (num == 1) then 
        !res = (x - (i - 2) * h) * (-pi) * cos(pi * x) * &
        !    (pi * sin(pi * y) * (j * h - y) - cos(pi * y)) / h**2
        res = (-1.0 / pi) * cos(pi * x) * (x - (i - 2) * h) * ( pi * sin(pi * y) * &
            (y - (j - 2) * h) + cos(pi * y) ) / h**2
    else if (num == 4) then
        !res = (x - (i - 2) * h) * (-pi) * cos(pi * x) * &
        !    (pi * sin(pi * y) * (y - h * (j - 2)) + cos(pi * y)) / h**2
        res = (1.0 / pi) * cos(pi * x) * (-x + (i - 2) * h) * ( pi * sin(pi * y) * &
            (-y + h * j) - cos(pi * y) ) / h**2    
    end if
end subroutine g_n_phi_nx


subroutine g_n_phi_ny(x, y, res) ! up
    use TParameters
    implicit none
    
    real*8 :: x, y, res
    integer :: i, j
    
    i = i_1
    j = j_1
    res = 0d0

    if (num == 1) then 
        !res = (y - (j - 2) * h) * (-pi) * cos(pi * y) * eps * &
        !    (pi * cos(pi * x) * (h * (i - 2) - x) + sin(pi * x)) / h**2
        res = (1.0 / pi) * eps * sin(pi * y) * (y - (j - 2) * h) * &
            ( sin(pi * x) - pi * cos(pi * x) ) * (x - (i - 2) * h) / h**2
        !print *, 'sin'
    else if (num == 2) then
        !res = (y - (j - 2) * h) * (-pi) * cos(pi * y) * eps * &
        !    (pi * cos(pi * x) * (x - i * h) - sin(pi * x)) / h**2
        res = pi * eps * sin(pi * y) * (y - (j - 2) * h) * &
            ( cos(pi * x) * (x - i * h) / pi - sin(pi * x) / (pi * pi) ) / h**2    
    end if
end subroutine g_n_phi_ny

subroutine g_d(x, y, res)
    use TParameters
    implicit none
    
    real*8 :: x, y, res
    
    res = sin(pi * x) * cos(pi * y)
    
end subroutine g_d

subroutine phi(x, y, res)
    use TParameters
    implicit none
    !   4|3     !
    !   1|2     !
    integer :: i, j
    real*8 :: x, y, res
    
    i = i_1
    j = j_1
    if (num == 1) then
        res = 1.0 * (x - (i - 1 - 1) * h) * (y - (j - 1 - 1) * h) / h**2
    else if (num == 2) then
        res = 1.0 * (-x + (i + 1 - 1) * h) * (y - (j - 1 - 1) * h) / h**2
    else if (num == 3) then
        res = 1.0 * (x - (i + 1 - 1) * h) * (y - (j + 1 - 1) * h) / h**2
    else if (num == 4) then
        res = 1.0 * (x - (i - 1 - 1) * h) * (-y + (j + 1 - 1) * h) / h**2
    end if
    
end subroutine phi

subroutine grad_phi_x(x, y, res)
    use TParameters
    implicit none
    
    integer :: i, j
    real*8 :: x, y, res
    
    i = i_1
    j = j_1
    if (num == 1) then
        res = 1.0 * (y - (j - 1 - 1) * h) / h**2
    else if (num == 2) then
        res = (-1.0) * (y - (j - 1 - 1) * h) / h**2
    else if (num == 3) then
        res = 1.0 * (y - (j + 1 - 1) * h) / h**2
    else if (num == 4) then
        res = 1.0 * (-y + (j + 1 - 1) * h) / h**2
    end if

end subroutine grad_phi_x

subroutine grad_phi_x_k_l(x, y, res)
    use TParameters
    implicit none
    
    integer :: i, j
    real*8 :: x, y, res
    
    i = k
    j = l
    if (num_k_l == 1) then
        res = 1.0 * (y - (j - 1 - 1) * h) / h**2
    else if (num_k_l == 2) then
        res = (-1.0) * (y - (j - 1 - 1) * h) / h**2
    else if (num_k_l == 3) then
        res = 1.0 * (y - (j + 1 - 1) * h) / h**2
    else if (num_k_l == 4) then
        res = 1.0 * (-y + (j + 1 - 1) * h) / h**2
    end if

end subroutine grad_phi_x_k_l

subroutine grad_phi_y(x, y, res)
    use TParameters
    implicit none
    
    integer :: i, j
    real*8 :: x, y, res
    
    i = i_1
    j = j_1
    if (num == 1) then
        res = 1.0 *(x - (i - 1 - 1) * h) / h**2
    else if (num == 2) then
        res = 1.0 * (-x + (i + 1 - 1) * h) / h**2
    else if (num == 3) then
        res = 1.0 * (x - (i + 1 - 1) * h) / h**2
    else if (num == 4) then
        res = (-1.0) * (x - (i - 1 - 1) * h) / h**2
    end if

end subroutine grad_phi_y

subroutine grad_phi_y_k_l(x, y, res)
    use TParameters
    implicit none
    
    integer :: i, j
    real*8 :: x, y, res
    
    i = k
    j = l
    if (num_k_l == 1) then
        res = 1.0 * (x - (i - 1 - 1) * h) / h**2
    else if (num_k_l == 2) then
        res = 1.0 * (-x + (i + 1 - 1) * h) / h**2
    else if (num_k_l == 3) then
        res = 1.0 * (x - (i + 1 - 1) * h) / h**2
    else if (num_k_l == 4) then
        res = (-1.0) * (x - (i - 1 - 1) * h) / h**2
    end if

end subroutine grad_phi_y_k_l

subroutine d_grad_grad(x, y, res)
    use TParameters
    implicit none
    
    real*8 :: x, y, res_1, res_2, res
    
    res_1 = 0d0
    res_2 = 0d0
    res = 0d0    

    call grad_phi_x_k_l(x, y, res_1)
    call grad_phi_x(x, y, res_2)
    res = res_1 * res_2
    call grad_phi_y_k_l(x, y, res_1)
    call grad_phi_y(x, y, res_2)
    res = res + eps * res_1 * res_2
    !d_grad_grad = grad_phi_x_k_l(x, y) * grad_phi_x(x, y) + eps * grad_phi_y_k_l(x, y) * grad_phi_y(x, y)

end subroutine d_grad_grad

subroutine g_n_x(x, y, res)
    use TParameters
    implicit none
    
    real*8 :: x, y, res
    
    res = (-1.0) * pi * cos(pi * x) * cos(pi * y)
end subroutine g_n_x

subroutine g_n_y(x, y, res)
    use TParameters
    implicit none
    
    real*8 :: x, y, res
    
    res = eps  * pi * sin(pi * x) * sin(pi * y)
end subroutine g_n_y


subroutine func_l_h(x, y, res)
    use TParameters
    implicit none
    
    integer :: i, j
    real*8 :: res, res_temp, x, y, res_g_d
    
    res = 0d0
    res_temp = 0d0
    res_g_d = 0d0
    i = i_1
    j = j_1
    num = 3
        ! i = i_1
	    ! j = j_1
        call phi(x, y, res_temp)
        !print *, res_temp, sol((j - 1) * (n + 1) + i)
        res = res + res_temp * sol((j_1 - 1) * (n + 1) + i_1)
    num = 2
        !i = i_1
        j_1 = j_1 + 1
        call phi(x, y, res_temp)
        res = res + res_temp * sol((j_1 - 1) * (n + 1) + i_1)
    num = 4
        i_1 = i_1 + 1
        j_1 = j_1 - 1 
        call phi(x, y, res_temp)
        res = res + res_temp * sol((j_1 - 1) * (n + 1) + i_1)
    num = 1
        i_1 = i_1 
        j_1 = j_1 + 1
        call phi(x, y, res_temp)
        res = res + res_temp * sol((j_1 - 1) * (n + 1) + i_1)
    call g_d(x, y, res_g_d)
    !print *, x, y, res
    i_1 = i
    j_1 = j
    res = (res - res_g_d)**2
	!print *, res
end subroutine
