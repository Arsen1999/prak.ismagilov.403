Module TParameters
    implicit none
	 
    integer :: i_1 ! current i
    integer :: j_1 ! current j
    integer :: k
    integer :: l
    integer :: num ! cell of current i.j
    integer :: num_k_l ! cell of current k.l
    integer :: n
    integer :: suitable_point ! necessary k.l point
    integer :: mode
    real*8  :: eps
    real*8  :: h
    real*8  :: x_k
    real*8  :: y_l
    !real*8  :: func_res ! res of func.f90
    real*8  :: pi = 4 * atan(1.0)    
    real*8, allocatable  :: sol(:)

    integer :: m = 9
    real*8  :: w_i(2)
    real*8  :: n_i(3, 9)

end Module TParameters
