program main
    use TParameters
    implicit none
    
    integer :: i, j, matx_pos, mat_row_num
    real*8, allocatable :: vrt(:, :), matx(:), b(:), vec_f_phi(:), &
            vec_g_n(:), vec_g_d(:), fpar(:), w(:)
    integer, allocatable :: matcol(:), matrow(:), ipar(:)
    real*8 :: x, y, scalar_f_phi, int_g_n_phi, scalar_d_grad_grad, temp_res, distdot, &
                    c_h_norm, err_c, l_h_norm, err_l
    
    !open(1, file = 'matX.txt', status = 'old')
    !open(2, file = 'matcol.txt', status = 'old')
    !open(3, file = 'matrow.txt', status = 'old')
    !open(4, file = 'b.txt', status = 'old')
    open(15, file = 'sol.txt', status = 'old')
    print *, "Enter n"
    read *, n
    print *, "Enter epsilon"
    read *, eps
    h = 1.0 / n    
    matx_pos = 1
    mat_row_num = 0
    call cube_integral_coeff()
    allocate(b((n + 1) *(n + 1)), vec_f_phi((n + 1) *(n + 1)), vec_g_n((n + 1) *(n + 1)), vec_g_d(n*n), sol((n + 1) *(n + 1)), &
                matx(n*n*n*n), matcol(n*n*n*n), matrow((n + 1) * (n + 1) + 1), ipar(16), fpar(16), w(7 * (n + 1) * (n + 1)))
    matrow(1) = 1 ! запирающий элемент
    matx = 0d0
    !vec_g_d = 0d0
    vec_g_n = 0d0
    vec_f_phi = 0d0
    b = 0d0
    do j_1 = 1, n + 1
        y = (j_1 - 1) * h 
        do i_1 = 1, n + 1
            x = (i_1 - 1) * h
            if ((j_1 == 1) .or. (i_1 == 1)) then ! g_d
                matx(matx_pos) = 1
                matcol(matx_pos) = (j_1 - 1) * (n + 1) + i_1
                matx_pos = matx_pos + 1
                call g_d(x, y, temp_res)
                b((j_1 - 1) * (n + 1) + i_1) = temp_res
            else 
                do l = 1, n + 1
		            do k = 1, n + 1
                        if ( (i_1 - k < 2) .AND. (i_1 - k > -2) .AND. (j_1 - l < 2) .AND. (j_1 - l > -2) ) then 
                            vec_f_phi((j_1 - 1) * (n + 1) + i_1) = scalar_f_phi(x, y)
                            vec_g_n((j_1 - 1) * (n + 1) + i_1) = int_g_n_phi(x, y)
		                    ! matx !
		                    mode = 1
		                    temp_res = scalar_d_grad_grad(x, y)
		                    if (suitable_point == 1) then
		                        matx(matx_pos) = temp_res
		                        matcol(matx_pos) = (l - 1) * (n + 1) + k 
		                        matx_pos = matx_pos + 1
		                        suitable_point = 0
                            !print *, matx_pos - 1, matx(matx_pos - 1), i_1, j_1, k, l
		                    end if
		                end if
		            end do
		        end do
            end if
            matrow((j_1 - 1) * (n + 1) + i_1 + 1) = matx_pos 
        end do
    end do

    print *, 'vec_f_phi'
    !print *, (i, vec_f_phi(i), i = 1, (n+1)*(n+1))
    print *, 'vec_g_n'
    b = b + vec_f_phi - vec_g_n
    
    print *, 'b'    

    ipar(1) = 0
    ipar(2) = 0
    ipar(3) = 1
    ipar(4) = 7 * (n + 1) * (n + 1)
    ipar(5) = 10
    ipar(6) = 5000
    fpar(1) = 1.0d-9
    fpar(2) = 1.0d-13
    sol = 0d0
    
    10  call bcg((n+1)*(n+1), b, sol, ipar, fpar, w)

    if (ipar(1).eq.1) then
        call amux((n+1)*(n+1), w(ipar(8)), w(ipar(9)), matx, matcol, matrow)
        goto 10
    else if (ipar(1).eq.2) then
        call atmux((n+1)*(n+1), w(ipar(8)), w(ipar(9)), matx, matcol, matrow)
        goto 10
    else if (ipar(1).le.0) then
        if (ipar(1).eq.0) then
            print *, 'Successfully.'
        else if (ipar(1).eq.-1) then
            print *, 'Iterative solver has iterated too many times.'
        else if (ipar(1).eq.-2) then
            print *, 'Iterative solver was not given enough work space.'
            print *, 'The work space should at least have ', ipar(4), ' elements.'
        else if (ipar(1).eq.-3) then
            print *, 'Iterative solver is facing a break-down.'
        else
            print *, 'Iterative solver terminated. code =', ipar(1)
        endif
    endif
    
    !print *, (i, C(i), i = 1, (n+1)*(n+1))
    err_c = c_h_norm(sol, n + 1) ! n - because inside function n->n*n
    err_l = l_h_norm(sol, n + 1)
    print *, err_c, err_l, ipar(7)
    do i_1 = 1, n + 1
        do j_1 = 1, n + 1
            write(15,*) (i_1-1)*h, (j_1-1)*h, sol((j_1 - 1) * (n + 1) + i_1)
        end do
    end do
    !close(1)
    !close(2)
    !close(3)
    !close(4)
    close(15)
    deallocate(b, vec_f_phi, vec_g_n, vec_g_d, sol, &
                matx, matcol, matrow, ipar, fpar, w)
end program main

function distdot(n, x, ix, y, iy)
    implicit none
    integer :: n, ix, iy
    real*8 :: distdot, x(*), y(*), ddot
    
    external ddot
    
    distdot = ddot(n, x, ix, y, iy)
    return
end function
